//
//  ConvertorController.swift
//  LivePhotoConvertor
//
//  Created by Apps4World on 12/12/19.
//  Copyright © 2019 Apps4World. All rights reserved.
//

import UIKit

// Main controller showing the progress
class ConvertorController: UIViewController {

    /* * * Instructions * * * /
     * 1) Drag & Drop your set of images and videos to 'Videos' folder
     * 2) Set the 'assetStartIndex' to the name of your first image/video set
     * 3) Run the app and wait for conversion to finish
     * 4) Go to the directory showed in the console to retrieve the files and drop them in the iLiveWallpapers project
     */
    
    @IBOutlet weak private var statusLabel: UILabel!
    @IBOutlet weak private var progressLabel: UILabel!
    
    /// First video index/name. !!! Must be a number !!!
    private var assetStartIndex: Int = 29
    private var didPrintDirectory: Bool = false
    
    /// Initial logic when screen loads
    override func viewDidLoad() {
        super.viewDidLoad()
        startConverting()
    }
    
    /// Starts video conversion to Live Photos
    private func startConverting() {
        convertAssets(atIndex: assetStartIndex) // convert first item
    }
    
    /// Convert assets by index
    private func convertAssets(atIndex index: Int) {
        if let photoURL = Bundle.main.url(forResource: "\(assetStartIndex)", withExtension: "jpg"),
            let videoURL = Bundle.main.url(forResource: "\(assetStartIndex)", withExtension: "mov") {
            A4WLivePhotoManager.shared.assetIndex = assetStartIndex
            A4WLivePhotoManager.generate(from: photoURL, videoURL: videoURL, progress: { progress in  })
            { (livePhoto, resources) in
                self.progressLabel.isHidden = false
                if livePhoto != nil, let resFiles = resources {
                    print("=== CONVERTED ITEM AT INDEX \(self.assetStartIndex)")
                    if self.didPrintDirectory == false {
                        print("\n=== CONVERTED FILES CAN BE FOUND HERE ===\n\(resFiles.pairedVideo.absoluteString.replacingOccurrences(of: "/\(self.assetStartIndex).MOV", with: ""))\n")
                        self.didPrintDirectory = true
                    }
                    self.statusLabel.text = "converting"
                    self.progressLabel.text = "file with index \(self.assetStartIndex)"
                    self.assetStartIndex += 1
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.7, execute: {
                        self.convertAssets(atIndex: self.assetStartIndex) })
                } else {
                    print("=== CAN NOT CONVERT FILE WITH INDEX \(self.assetStartIndex) ===")
                    self.statusLabel.text = "error"
                    self.progressLabel.text = "file with index \(self.assetStartIndex)"
                }
            }
        } else {
            print("=== CAN'T LOAD FILE WITH INDEX \(assetStartIndex) ===")
            self.statusLabel.text = "complete"
            self.progressLabel.text = "can't load file \(self.assetStartIndex)"
        }
    }
}

