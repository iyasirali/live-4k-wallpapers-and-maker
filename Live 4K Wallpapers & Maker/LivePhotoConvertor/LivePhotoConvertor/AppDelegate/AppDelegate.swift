//
//  AppDelegate.swift
//  LivePhotoConvertor
//
//  Created by Apps4World on 12/12/19.
//  Copyright © 2019 Apps4World. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
}

