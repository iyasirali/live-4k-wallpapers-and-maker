//
//  HomeViewController.swift
//  iLiveWallpapers
//
//  Created by Apps4World on 12/4/19.
//  Copyright © 2019 Apps4World. All rights reserved.
//

import UIKit
import Photos
import PhotosUI
import MobileCoreServices
import StoreKit

// This is the main home screen that user see when app launches
class HomeViewController: LibraryAccessViewController {

    @IBOutlet weak var livePhotoContainer: UIView!
    @IBOutlet weak var buildButton: UIButton!
    @IBOutlet weak var galleryButton: UIButton!
    @IBOutlet weak var instructions: UILabel!
    private let manager = LivePhotoManager()
    private var livePhotoView = PHLivePhotoView()
    
    /// Initial logic when the screen loads
    override func viewDidLoad() {
        super.viewDidLoad()
        livePhotoView = PHLivePhotoView(frame: view.frame)
        livePhotoContainer.addSubview(livePhotoView)
        buildButton.applyCornerRadius()
        galleryButton.applyCornerRadius()
        loadLivePhoto()
       // print("is subscribed \(UserRepo.shared.isUserSubscribed)")
    }

    /// When user selects build option
    @IBAction func buildAction(_ sender: UIButton) {
        
        if let _ = UserRepo.shared.isTransactionIdentifierExist {
            openPhotoAuthorization()
        } else {
            startSubscription()
            
        }
       
        
    /*    requestPhotoLibraryAuthorization { (success) in
            DispatchQueue.main.async { if success { self.presentImageVideoPicker() }}
        } */
    }
    
    /// Present image picker / gallery
    private func presentImageVideoPicker() {
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.allowsEditing = true
        picker.sourceType = .savedPhotosAlbum
        picker.mediaTypes = [kUTTypeMovie as String]
        present(picker, animated: true, completion: nil)
    }
    
    /// Load live photos
    @objc private func loadLivePhoto() {
        guard let photoURL = Bundle.main.url(forResource: "20", withExtension: "JPG"),
            let liveURL = Bundle.main.url(forResource: "20", withExtension: "MOV")
        else { return }
        manager.loadLivePhoto(photoURL: photoURL, videoURL: liveURL) { (livePhoto) in
            DispatchQueue.main.async {
                LoadingView.removeLoadingView()
                if let photo = livePhoto {
                    self.livePhotoView.livePhoto = photo
                    self.livePhotoView.startPlayback(with: .full)
                }
            }
        }
    }
    
    /// Launch build screen after user selected a video
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true) {
            if let url = info[UIImagePickerController.InfoKey.mediaURL] as? URL {
                if let build = self.storyboard?.instantiateViewController(withIdentifier: "build") as? BuildViewController {
                    build.videoURL = url
                    self.present(build, animated: true, completion: nil)
                }
            }
        }
    }
    
    func openPhotoAuthorization() {
        requestPhotoLibraryAuthorization { (success) in
            DispatchQueue.main.async { if success { self.presentImageVideoPicker() }}
        }
    }
    
    var productsArray = [SKProduct]()
    var productIDs: [String] = ["com.nk.Live4KWallpapersMaker"]
    
    func startSubscription() {
        
       /* if let receiptURL = Bundle.main.appStoreReceiptURL {
            if let receipt = try? Data(contentsOf: receiptURL) {
                let json = try? JSONSerialization.jsonObject(with: receipt, options: .allowFragments) as? [String:Any]
                let string = String.init(data: receipt, encoding: .ascii)
                print(string)
            }
        } */
        
        if (SKPaymentQueue.canMakePayments()) {
//            let productID = NSSet(array: self.productIDs)
            let productID = Set(productIDs)
            let productsRequest = SKProductsRequest(productIdentifiers: productID)
            productsRequest.delegate = self
            productsRequest.start()
        } else {
            print("can't make purchases");
        }
    }
    
    func buyProduct(product: SKProduct) {
        //println("Sending the Payment Request to Apple")
        let payment = SKPayment(product: product)
        SKPaymentQueue.default().add(payment)
    }
}

extension HomeViewController: SKProductsRequestDelegate, SKPaymentTransactionObserver {
    // SKProductRequest Delegate
    
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        guard let product = response.products.first else {
            return
        }
        self.buyProduct(product: product)
    }
    
    public func request(_ request: SKRequest, didFailWithError error: Error) {
        print("Failed to load list of products.")
        print("Error: \(error.localizedDescription)")
    }
    
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        
        
        
        for transaction:AnyObject in transactions {
            if let trans:SKPaymentTransaction = transaction as? SKPaymentTransaction{
                
                let state = trans.transactionState
                
                switch state {
                case .purchased,.restored:
                    print("Product Purchased")
                    
                    //Do unlocking etc stuff here in case of new purchase
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                    if let id = trans.transactionIdentifier {
                        UserRepo.shared.isTransactionIdentifierExist = id
                        openPhotoAuthorization()
                    }
                case .failed:
                    print("Purchased Failed");
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                    UserRepo.shared.isTransactionIdentifierExist = nil
//                case .restored:
//                    print("Already Purchased")
//                    //Do unlocking etc stuff here in case of restor
//                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
//                    if let id = trans.transactionIdentifier {
//                        UserRepo.shared.isTransactionIdentifierExist = id
//                        openPhotoAuthorization()
//                    }
                default:
                    print("Prdouct State \(state)")
                    UserRepo.shared.isTransactionIdentifierExist = nil
                    break
                }
            }
        }
    }
    
    
    //If an error occurs, the code will go to this function
    func paymentQueue(_ queue: SKPaymentQueue, restoreCompletedTransactionsFailedWithError error: Error) {
        // Show some alert
    }
}

