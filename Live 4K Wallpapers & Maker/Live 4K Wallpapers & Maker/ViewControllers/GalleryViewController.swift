//
//  GalleryViewController.swift
//  iLiveWallpapers
//
//  Created by Apps4World on 12/4/19.
//  Copyright © 2019 Apps4World. All rights reserved.
//

import UIKit
import Photos
import PhotosUI
import GoogleMobileAds

// This is the gallery with all live wallpapers from the app
class GalleryViewController: LibraryAccessViewController, UIScrollViewDelegate {

    @IBOutlet weak private var scrollView: UIScrollView!
    @IBOutlet weak private var saveButton: UIButton!
    @IBOutlet weak private var closeButton: UIButton!
    private let manager = LivePhotoManager()
    private var livePhotoView = PHLivePhotoView()
    private var currentWallpaperIndex = 0
    private let adsDisplayInterval = 4
    private var interstitial: GADInterstitial!
    
    /// Initial logic when the screen loads
    override func viewDidLoad() {
        super.viewDidLoad()
        setupSaveButtonStyle()
        setupLiveWallpapersScrollView()
        prepareInterstitialAd()
    }
    
    /// Prepare the AdMob interstitial and load the ad request
    private func prepareInterstitialAd() {
        interstitial = GADInterstitial(adUnitID: "ca-app-pub-3940256099942544/4411468910")
        let request = GADRequest()
        interstitial.load(request)
    }

    /// Setup save button style
    private func setupSaveButtonStyle() {
        saveButton.layer.borderWidth = 1.5
        saveButton.layer.borderColor = UIColor.white.cgColor
        saveButton.layer.cornerRadius = saveButton.frame.height/2
    }
    
    /// Setul live wallpapers into a scroll view
    private func setupLiveWallpapersScrollView() {
        for livePhotoIndex in 0..<INT_MAX {
            if let image = UIImage(named: "\(livePhotoIndex).JPG") {
                let frame = CGRect(x: CGFloat(livePhotoIndex)*view.frame.width, y: 0, width: view.frame.width, height: view.frame.height)
                let imageView = UIImageView(frame: frame)
                imageView.contentMode = .scaleAspectFill
                imageView.image = image
                scrollView.addSubview(imageView)
            } else { break }
        }
        let count = scrollView.subviews.count
        scrollView.contentSize = CGSize(width: view.frame.width*CGFloat(count), height: view.frame.height)
        livePhotoView = PHLivePhotoView(frame: view.frame)
        scrollView.addSubview(livePhotoView)
        loadLivePhoto()
    }
    
    /// Load live photos
    private func loadLivePhoto() {
        guard let photoURL = Bundle.main.url(forResource: "\(currentWallpaperIndex)", withExtension: "JPG"),
            let liveURL = Bundle.main.url(forResource: "\(currentWallpaperIndex)", withExtension: "MOV")
        else { return }
        manager.loadLivePhoto(photoURL: photoURL, videoURL: liveURL) { (livePhoto) in
            DispatchQueue.main.async {
                LoadingView.removeLoadingView()
                if let photo = livePhoto {
                    self.livePhotoView.frame = CGRect(x: self.scrollView.contentOffset.x, y: 0, width: self.livePhotoView.frame.width, height: self.livePhotoView.frame.height)
                    self.livePhotoView.livePhoto = photo
                    self.livePhotoView.startPlayback(with: .full)
                }
            }
        }
    }
    
    /// Save current live wallpaper to the gallery
    private func saveCurrentLiveWallpaper() {
        guard let photoURL = Bundle.main.url(forResource: "\(currentWallpaperIndex)", withExtension: "JPG"),
            let liveURL = Bundle.main.url(forResource: "\(currentWallpaperIndex)", withExtension: "MOV") else { return }
        manager.saveLivePhoto(photoURL: photoURL, videoURL: liveURL) { (success) in
            DispatchQueue.main.async {
                LoadingView.removeLoadingView()
                if success { self.presentLivePhotoSavedAlert() }
                else { self.presentGenericErrorAlert() }
            }
        }
    }
    
    /// When scroll view ends animation
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        currentWallpaperIndex = Int(scrollView.contentOffset.x / scrollView.frame.size.width)
        loadLivePhoto()
        if currentWallpaperIndex % adsDisplayInterval == 0, interstitial?.isReady ?? false {
            interstitial.present(fromRootViewController: self)
            prepareInterstitialAd()
        }
    }
    
    /// Invoke save action
    @IBAction private func saveAction(_ sender: UIButton) {
        requestPhotoLibraryAuthorization { (success) in
            if success { self.saveCurrentLiveWallpaper() }
        }
    }
    
    /// Hide/Show save button
    @IBAction private func showHideSaveButton(_ sender: UITapGestureRecognizer) {
        UIView.animate(withDuration: 0.5) {
            self.saveButton.alpha = self.saveButton.alpha == 0 ? 1 : 0
            self.closeButton.alpha = self.saveButton.alpha
        }
    }
    
    /// Close gallery and go back to main screen
    @IBAction private func closeGallery(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
}
