//
//  AppDelegate.swift
//  iLiveWallpapers
//
//  Created by Apps4World on 12/4/19.
//  Copyright © 2019 Apps4World. All rights reserved.
//

import UIKit
import GoogleMobileAds
import StoreKit
import AppReceiptValidator

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        GADMobileAds.sharedInstance().start(completionHandler: nil)
        if let id = UserRepo.shared.isTransactionIdentifierExist {
            switch verify(transactionIdentifier: id) {
            case .success(_):
                print("receipt found")
            case .failure(let error):
                switch error {
                case IAPManagerError.subscriptionExpired:
                    UserRepo.shared.isTransactionIdentifierExist = nil
                default:
                    print("found error")
                }
            }
        }
        return true
    }
    
    func verify(transactionIdentifier: String) -> Result<InAppPurchaseReceipt,Error> {
            let validation = AppReceiptValidator().validateReceipt()
            switch validation {
            case .success(let reciept,_,_):
                guard let inAppPurchaseReceipt = reciept.inAppPurchaseReceipts.last, inAppPurchaseReceipt.transactionIdentifier == transactionIdentifier else {
                    return .failure(IAPManagerError.transactionReceiptNotFound)
                }

                let expiry = inAppPurchaseReceipt.subscriptionExpirationDate?.timeIntervalSince1970 ?? 0.0

                if expiry >= Date().timeIntervalSince1970 {
                    return .success(inAppPurchaseReceipt)
                }else {
                    return .failure(IAPManagerError.subscriptionExpired)
                }

            case .error(let error,_, _):
                return .failure(error)
            }
        }
}

public final class UserRepo {
    
    private let keyTransactionIdentifier = "keyIsSubscribed"

    private let defaults: UserDefaults = UserDefaults.standard
    
    var isTransactionIdentifierExist: String? {
        set{
            defaults.set(newValue, forKey: keyTransactionIdentifier)
        }
        get{
           return defaults.value(forKey: keyTransactionIdentifier ) as? String
        }
    }
    
    public static let shared = UserRepo()
    
    private init() {
        
    }
}

enum IAPManagerError: Error {
        ///It indicates that the product identifiers could not be found
        case noProductIDsFound
        ///No IAP products were returned by the App Store because none was found
        case noProductsFound
        ///The user cancelled an initialized purchase process
        case paymentWasCancelled
        ///The app cannot request App Store about available IAP products for some reason.
        case productRequestFailed
        ///in the receipt there was no entry for the transaction
        case transactionReceiptNotFound
        case subscriptionExpired
    }
