//
//  LivePhotoManager.swift
//  iLiveWallpapers
//
//  Created by Apps4World on 12/4/19.
//  Copyright © 2019 Apps4World. All rights reserved.
//

import UIKit
import Photos
import PhotosUI
import MobileCoreServices

// Image/Video assets will be stored temporarily at this path
struct FilePaths {
    static let documentsPath = NSSearchPathForDirectoriesInDomains(.cachesDirectory,.userDomainMask,true)[0] as AnyObject
    static var liveAssetPath = FilePaths.documentsPath.appending("/")
}

// Main Live Photo manager to save/load live phoots
class LivePhotoManager: NSObject {

    /// Load video from URL (gallery), convert it, take a screenshot of it, save both image/video at file paths
    func loadVideoWithVideoURL(_ videoURL: URL, completion: @escaping (_ livePhoto: PHLivePhoto?) -> Void) {
        DispatchQueue.main.async { LoadingView.showLoadingView() }
        let asset = AVURLAsset(url: videoURL)
        let generator = AVAssetImageGenerator(asset: asset)
        generator.appliesPreferredTrackTransform = true
        let time = NSValue(time: CMTimeMakeWithSeconds(CMTimeGetSeconds(asset.duration)/2, preferredTimescale: asset.duration.timescale))
        generator.generateCGImagesAsynchronously(forTimes: [time]) { _, image, _, _, _ in
            if let image = image, let data = UIImage(cgImage: image).pngData() {
                let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
                let imageURL = urls[0].appendingPathComponent("image.jpg")
                try? data.write(to: imageURL, options: [.atomic])
                
                let image = imageURL.path
                let mov = videoURL.path
                let output = FilePaths.liveAssetPath
                let assetIdentifier = UUID().uuidString
                
                let _ = try? FileManager.default.createDirectory(atPath: output, withIntermediateDirectories: true, attributes: nil)
                do {
                    try FileManager.default.removeItem(atPath: output + "/IMG.JPG")
                    try FileManager.default.removeItem(atPath: output + "/IMG.MOV")
                } catch { }
                
                ImageAssetManager(path: image).write(output + "/IMG.JPG", assetIdentifier: assetIdentifier)
                VideoAssetManager(path: mov).write(output + "/IMG.MOV", assetIdentifier: assetIdentifier)
                
                self.loadLivePhoto(photoURL: URL(fileURLWithPath: FilePaths.liveAssetPath + "/IMG.JPG"),
                                   videoURL: URL(fileURLWithPath: FilePaths.liveAssetPath + "/IMG.MOV"),
                                   completion: completion)
                
            } else { LoadingView.removeLoadingView(); completion(nil) }
        }
    }
    
    /// Loads a live photo from two given URLs
    func loadLivePhoto(photoURL: URL, videoURL: URL, completion: @escaping (_ livePhoto: PHLivePhoto?) -> Void) {
        DispatchQueue.main.async {
            PHLivePhoto.request(withResourceFileURLs: [photoURL, videoURL], placeholderImage: nil, targetSize: UIScreen.main.bounds.size, contentMode: PHImageContentMode.aspectFit, resultHandler: { (livePhoto, info) -> Void in completion(livePhoto) })
        }
    }
    
    /// Save live photo to gallery
    func saveLivePhoto(photoURL imageURL: URL? = nil, videoURL: URL? = nil, completion: @escaping (_ success: Bool) -> Void) {
        DispatchQueue.main.async {
            LoadingView.showLoadingView()
            let photoURL = imageURL ?? URL(fileURLWithPath: FilePaths.liveAssetPath + "/IMG.JPG")
            let liveURL = videoURL ?? URL(fileURLWithPath: FilePaths.liveAssetPath + "/IMG.MOV")
            PHPhotoLibrary.shared().performChanges({ () -> Void in
                let creationRequest = PHAssetCreationRequest.forAsset()
                let options = PHAssetResourceCreationOptions()
                creationRequest.addResource(with: PHAssetResourceType.pairedVideo, fileURL: liveURL, options: options)
                creationRequest.addResource(with: PHAssetResourceType.photo, fileURL: photoURL, options: options)
            }, completionHandler: { (success, error) -> Void in LoadingView.removeLoadingView(); completion(success) })
        }
    }
}
